#!/bin/bash

# SDEV-415-81: Linux/ Unix Programming I
# Week14/15 Assignment: Final Project 3, lookup_menu.sh

# Petra Gates
# 4/29/2021
# This Script should:
#   - check if datafile.txt exists and is readable/writable
#   - offer the user a menu of choices
#     - add entry
#       - check for preexisting entry
#       - inform user if already present
#     - delete entry
#       - check for existence of entry
#       - inform user if not present
#     - view entry
#     - exit
#       - use a digit to indicate appropriate exit status
#   - handle options using functions
#   - ask if the user would like to see the menu again
#   - display, "Invalid entry, try again," and redisplay menu

file=~/datafile.txt

#############
# Functions #
#############

add_entry()
{
  echo "Enter name (First Last): "
  read -r fullName

  # testing to see if name already exists in file
  grep -iq "${fullName}" "${file}"
  entry_in_file=$?
  if [ "${entry_in_file}" -eq 0 ]; then
    echo "${fullName} already in file."
    call_menu_again
    return
  fi

  echo "Enter phone number (xxx-xxx-xxxx): "
  read -r phone
  echo "Enter address: "
  read -r address
  echo "Enter birth date (x/xx/xxxx): "
  read -r dob
  echo "Enter salary (xxxxx): "
  read -r salary

  # assigning user entered field values to variable, adding field separators
  added_entry="${fullName}:${phone}:${address}:${dob}:${salary}"

  # appending new entry to datafile
  echo "${added_entry}" >> "${file}"
  add_result=$?
  if [ "${add_result}" -eq 0 ]; then
    echo
    echo "Entry added."

    # sort alphabetically by 2nd word (last name, using defualt IFS <space>),
    # redirect to tmp file, move to original file location
    sort -dk2 "${file}" > tmp && mv tmp "${file}"

    # display newly added entry preceded by line number
    grep -n "${added_entry}" "${file}"
  else
    echo "Error adding new entry."
  fi

  call_menu_again
}

delete_entry()
{
  echo "Enter the name for the entry you would like to delete: "
  read -r name_to_delete
  grep -iq "${name_to_delete}" "${file}"
  name_present_status=$?
  if [ "${name_present_status}" -ne 0 ]; then
    echo "${name_to_delete} not found in ${file}."
    call_menu_again
    return
  else
    sed -i "/${name_to_delete}/d" "${file}"
    echo "${name_to_delete} deleted from ${file}."
  fi

  call_menu_again
}

view_entry()
{
  sort -dk2 "${file}" > tmp && mv tmp "${file}"

  echo "Type the name you would like to view (First Last), or 'all': "
  read -r view_name
  if [[ "${view_name}" == 'all' ]]; then
    echo
    echo "datafile.txt contents, sorted alphabetically by last name:"
    echo "----------------------------------------------------------"
    lines=$(cat "${file}")
    for line in "${lines}"
    do
      echo "${line}"
    done
    echo
    # display number of lines in datafile.txt
    numLines=$(wc -l < "${file}")
    echo "Number of entries: ${numLines}"
  else
    grep -in "${view_name}" "${file}"
    view_status=$?
    if [ "${view_status}" -ne 0 ]; then
      echo "${view_name} not found."
    fi
    echo
  fi

  call_menu_again
}

exit_lookup()
{
  echo "Exiting program."
          echo
          exit
}

menu()
{
  if [ -r "${file}" ] && [ -w "${file}" ]; then
    select menu_item in "Add entry" "Delete entry" "View entry" "Exit"
    do
      case ${menu_item} in
        "Add entry")
          add_entry
          ;;
        "Delete entry")
          delete_entry
          ;;
        "View entry")
          view_entry
          ;;
        "Exit")
          exit_lookup
          ;;
        *)
        echo "Invalid entry, try again."
        menu
        ;;
      esac
    done
  else
    exit_status=$?
    echo "File does not exist, is not readable, or is not writable."
    echo "Exiting with status: ${exit_status}"
    exit "${exit_status}"
  fi
}

call_menu_again()
{
  echo "Would you like to see the menu again? (yes/y)"
  echo "(<ENTER> to skip menu)"
  read -r menu_again
  if [[ "${menu_again}" == 'yes' || "${menu_again}" == 'y' ]]; then
    menu
  fi
}

######################
# Call menu function #
######################
while true
do
  menu
done
